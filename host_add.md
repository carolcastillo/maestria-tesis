##1.6 Questions of the study

1.3 Is the biological composition of Pimplinae the same at different altitudes (spectrum of host) ?

##3.3 Biological composition at different altitudes

The biological composition at 2800 m.a.s.l. was different than the composition
at the other elevations (Table 4). The percentages of idiobiont parasitoids of
deeply and weakly concealed insect is much lower than other elevations. Also,
the percentages of koinobionts ectoparasitoids of spiders is very much higher
than the other altitudes, they are almost half of the species found at this
elevation. At 2800 m.a.s.l the traps did not catch any individuals of
idiobiont ectoparasitoids/pseudoparasitoids of spiders egg sacs. The species
composition was very similar at 2000, 1500 and 800 m.a.s.l.

The temporal variation of the biological strategies at 2800 and 1500 m.a.s.l
seem to follow the same temporal variation of the species abundances presented
above (Fig. 7). They presented an increment in abundances in October and in
January at 2800 and 1500 m.a.s.l. respectively. However, the temporal
synchronization between strategies at 1500 m.a.s.l. was not significative but,
a random scenario where all the strategies had the highest abundances at one
sampling time is very rare (``p = 0.008``). Also, a temporal synchronization
between strategies abundances at 2800 m.a.s.l. was not statistically
significant.

![Temporal variation of biological strategies at 2800 and 1500 m.a.s.l.](figuras/F7_host.pdf)


##4.3 Biological composition at different altitudes

The differences in biological composition between 2800 m.a.s.l and the other
elevations did not add explanation to the different temporal patterns observed
between 2800 and 1500 m.a.s.l. Even though the proportion of koinobiont
species is higher at 2800 m.a.s.l. it follow a similar temporal pattern than
the idiobiont strategy at that elevation (although not statistically
supported).

In the Kosñipata valley, the species belonging to the idiobiont strategy that
attack weakly concealed pupae of cocoon insects are probably the ones that
determine the temporal variation of the Pimplinae group. Those strategies
posses the highest number of species and abundances, and there was not a
temporal synchronization between all the strategies of the group.

There is an apparent difference between the biological composition  of species
at higher altitudes compared to lower altitudes. There is a much higher
proportion of koinobionts ectoparasitoids of spiders and a slightly lower
proportion of the idiobiont parasitoid species that attack deeply and weakly
concealed insect host at 2800 m.a.s.l. compared to all the other altitudes
sampled in this study and the study of @Saaksjarvi2004 in a lowland Amazonia
rainforest in Peru (Table. ?). More surveys over the higher elevations of
tropical mountains are necessary to corroborate these results.

library("splines")
library("survival")
library("fitdistrplus")

votos <- c()

for(j in 1:9999) {
data <- matrix(nrow=32, ncol=5)

for( i in 1:32 ) {
  row <- c()
  # random value between 0 and 1
  x <- runif(1, 0, 1)
if ( x < 0.2 ) {
  row <- c(row, 1)
} else {
  row <- c(row, 0)
}
if ( x < 0.4 ) {
  row <- c(row, 1)
} else {
  row <- c(row, 0)
}
if ( x < 0.6 ) {
  row <- c(row, 1)
} else {
  row <- c(row, 0)
}
if ( x < 0.8 ) {
  row <- c(row, 1)
} else {
  row <- c(row, 0)
}
if ( x <= 1 ) {
  row <- c(row, 1)
} else {
  row <- c(row, 0)
}
  
  data[i,] <- row
}

sumas <- c()
for( i in 1:32) {
  sumas <- c(sum(data[i,]), sumas)
}
  votos <- c(max(tabulate(sumas)), votos)
}


n<- votos>=21
(sum(n)+1)/(9999+1)#probability


## 1. Introduction

*The distribution of organisms in space and time has always intrigued human
kind.* Despite the urge to exploit and modify nature and resources, the
description of plants and animals distribution over the territory and seasons
must have been present since early times, either for knowledge's sake or for
managing purposes, to maximaze the explotation of the land.

*Europeans started the exploration of earth during the 18th century and with
them the documentation and the early biogeographic studies began.* The
ecological changes along tropical mountains called the attention of some of
the most famous biologist of the 19th century, Humboldt and Darwin and were
used to assert new evolutive and biogeographical theories (Lomolino 2001,
Mccain 2004). However, after all those years of work and despite the
recognized importance of insects on the ecosystem, the species distribution of
insects along tropical mountains remains obscure.

The documentation of temporal variation in the tropics is scarse (Rosenzweig
1995). This is true for tropical birds and mammals as well as for some
insects groups with low densities on the collecting traps and high richness,
such as parasitoid wasps or spiders. As temporal variation of species
reflects ecological processes and are key to study diversity (Rosenzweig
1995), temporal variation research should be complementary to local richness
surveys and geographical patterns studies (White et al. 2010).

### 1.1 The study group

*The family Ichneumonidae is one of the most species rich family of insects in
the world. Townes (1969) estimated 60000 species and Gauld (1997) estimated
more than 100000 species. The subfamily is divided in 37 subfamilies (Gauld
1997) and in the Neotropics 30 subfamilies are present according to Fernández
& Sharkey (2006). In Peru, 20 subfamilies were reported 1 in the last
checklist (Rodríguez-Berrío et al. 2009) but, in the past few years, 5
subfamilies were registered to Peru for the first time (Alvarado et al. 2011;
Castillo et al. 2011; Alvarado 2012; Castillo et al. 2014; Veijalainen et al.
2014). Even though, most of the Ichneumonidae wasps are understudied,
undescribed, and might represent a great taxonomical challenge, Gauld et al.
(1998) recommended their study for being easier to monitor for long periods of
time and more responsive to sudden changes of the environment.* Rodríguez-
Berrío et al. (2009) reported 343 species to Peru but in just a few years 26
new species have been described in the country (Palacio et al. 2010; Alvarado
et al. 2011; Broad et al. 2011; Castillo et al. 2011; Alvarado 2012; Alvarado
et al. 2013a,b; Castillo et al. 2014; Gómez et al. 2014).

\pagebreak

## 2.3 Analysis

*Rarefaction curves were computed to compare the richness between altitudes
taking into account the number of individuals collected. To observe
differences in species composition between altitudes I performed a non metric
multidimensional scaling ordination (NMDS). 12 The ordination was created
using Bray-Curtis dissimilarities matrices of quantitative data.* 

To quantify the species composition variation that is explained by altitudinal
differences I used Multiple Regression on distance Matrices and the Mantel
test. Both methods model relationship of distance matrices using linear
regression. The three matrices emploid were species composition variation,
altitude differences and geographic distances.  Geographical distances were
included to take into account the natural levels of horizontal dissimilarity
(and the spatial autocorrelation between altitude and geographical distances).
For the geographical distance matrix the distances were transformed to
logarithm to linearize the species-spatial relationship described by Hubbell
(2001). The matrix of species composition variation was the same employed in
the NMDS ordination and the matrix of altitude differences did not required
any transformation.

Using Multiple Regression I quantified the amount of species composition
variation that is explained by altitude and geographical distances at the same
time. To quantify the amount of variation explained by altitude and
geographical distances separately I used the Mantel test.

The temporal variation of abundances was analyzed only  at 1500 and 2800
m.a.s.l. because the sampling there was the most complete and comparable. Due
to the variation of the number of collecting days at each sampling time and in
order to compare their abundances I transformed the number of individuals per
species per sample. The number of individuals per species per sample was
reduced in respect to the sample with the fewest Malaise trap days (27 days).

The abundances per species at each sampling time was divided between the
coefficient of:

number of malaise trap days/27

*The figures 6 and 8 in this study are using this modified abundance number and
for simplicity are referred as number of individuals or abundance.*

To test if the highest abundance between the five sampling times was created
by the added abundances of many species and was not randomly created, I
analyzed the temporal synchrony of species abundances. *I estimated the
statistical significance of the ‘temporal synchrony on the abundance peak
between species’ using a Monte Carlo permutation. I used ‘single species most
abundant sample time’ as the permutable units. The probability of error of
‘the sampling time with highest frequency of species’ was estimated by
comparing its value to 9999 values obtained by permutation in the simulated
sample (see Appendix 1). The same procedure was employed to test the temporal
synchrony between the biological strategies abundances of the subfamily
Pimplinae at 1500 and 2800 m.a.s.l.*

\pagebreak

# 3. Results 

*A total of 1098 individuals in 125 species and 19 genera were collected
(Table 2). The total number of MTM placed and collected was 53 but 9 MTM did
not catch Pimplinae individuals. The number of collected individuals per MTM
varies greatly (mean = 20.7, SD = 21.6). The most abundant genera were Pimpla
(606 ind.), Neotheronia (182 ind.) and Zonopimpla (102 ind.) making up to 81%
of the total individuals.* The species accumulation curve at all altitudes did
not reach a plateau (Fig. 2) and more sampling would add more species to the
survey rapidly. At all altitudes the accumulation curve had a similar shape,
except at 800 m.a.s.l. *The estimators at each altitude predicted similar
number of species* but *at 800 m.a.s.l. the very steep accumulation curves for
both estimators and their discrepancy in the predicted number of species
showed a rather insufficient sampling (Table 3). At 2800 and 2000 m.a.s.l. the
estimations levelled off showing a more reliable estimate of richness at these
sites.*

## 3.1 Richness and species composition variation between different altitudes

The rarefaction curves showed that at 1500 m.a.s.l. the number of species was
higher than other altitudes even if reducing the sampling. This was also the
case taking into account the 95% CI of the rarefaction curve at 1500 m.a.s.l.

The NMDS ordination grouped closer the species composition variation of
Malaise traps belonging to the same collecting altitude (Fig. 5). There was an
ordination along the second axis that reflected an altitudinal gradient, an
increase in elevation from top to bottom. But, the species composition
variation was related to at least another factor. The second factor was
represented by an ordination along the first axis. Because of this, I analyzed
the species composition variation also in relation to geographical distances.


\select@language {english}
\contentsline {section}{1. Introduction}{1}{section*.1}
\contentsline {subsection}{1.1 The study {\color {red}\sout {organism}}{\color {blue}\uwave {group}}}{2}{subsection*.2}
\contentsline {subsection}{1.2 Tropical insect seasonality and parasitoid activity}{3}{subsection*.3}
\contentsline {subsection}{1.3 Altitudinal richness patterns}{7}{subsection*.4}
\contentsline {subsection}{1.4 Insect diversity and conservation of tropical Andes}{9}{subsection*.5}
\contentsline {subsection}{{\color {red}\sout {1.5 Aim of the study}}}{10}{subsection*.6}
\contentsline {subsection}{{\color {blue}\uwave {1.5 Aim and questions of the study}}}{11}{subsection*.7}
\contentsline {subsection}{{\color {red}\sout {1.6 Questions of the study}}}{11}{subsection*.8}
\contentsline {section}{2. Materials and methods}{12}{section*.9}
\contentsline {subsection}{2.1. Study area and data}{12}{subsection*.10}
\contentsline {subsection}{2.2 Sampling}{15}{subsection*.11}
\contentsline {subsection}{2.3 Analysis}{15}{subsection*.12}
\contentsline {section}{3. Results}{19}{section*.13}
\contentsline {subsection}{3.1 Richness and {\color {blue}\uwave {variation in }}species composition {\color {red}\sout {variation }}between different altitudes}{24}{subsection*.14}
\contentsline {subsection}{3.2 Temporal variation}{26}{subsection*.15}
\contentsline {subsection}{3.3 {\color {red}\sout {Biological composition at different altitudes}}{\color {blue}\uwave {Temporal and altitudinal variation of number of individuals between parasitoid strategies}}}{28}{subsection*.16}
\contentsline {section}{4. Discussion}{32}{section*.17}
\contentsline {subsection}{4.1 Altitudinal patterns of richness}{32}{subsection*.18}
\contentsline {subsection}{4.2 Temporal patterns and {\color {red}\sout {biological }}composition {\color {blue}\uwave {of parasitoid strategies }}at different altitudes}{36}{subsection*.19}
\contentsline {section}{5. Bibliograhy}{40}{section*.20}
\contentsline {section}{6. Appendix}{45}{section*.21}

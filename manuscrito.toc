\select@language {english}
\contentsline {section}{1. Introduction}{1}{section*.1}
\contentsline {subsection}{1.1 The study group}{1}{subsection*.2}
\contentsline {subsection}{1.2 Tropical insect seasonality and parasitoid activity}{3}{subsection*.3}
\contentsline {subsection}{1.3 Altitudinal richness patterns}{5}{subsection*.4}
\contentsline {subsection}{1.4 Insect diversity and conservation of tropical Andes}{7}{subsection*.5}
\contentsline {subsection}{1.5 Aim and questions of the study}{8}{subsection*.6}
\contentsline {section}{2. Materials and methods}{9}{section*.7}
\contentsline {subsection}{2.1. Study area and data}{9}{subsection*.8}
\contentsline {subsection}{2.2 Sampling}{12}{subsection*.9}
\contentsline {subsection}{2.3 Analysis}{12}{subsection*.10}
\contentsline {section}{3. Results}{15}{section*.11}
\contentsline {subsection}{3.1 Richness and variation in species composition between different altitudes}{19}{subsection*.12}
\contentsline {subsection}{3.2 Temporal variation}{22}{subsection*.13}
\contentsline {subsection}{3.3 Temporal and altitudinal variation of abundance of individuals between parasitoid strategies}{23}{subsection*.14}
\contentsline {section}{4. Discussion}{27}{section*.15}
\contentsline {subsection}{4.1 Altitudinal patterns of richness}{27}{subsection*.16}
\contentsline {subsection}{4.2 Temporal patterns and composition of parasitoid strategies at different altitudes}{29}{subsection*.17}
\contentsline {section}{Acknowledgements}{33}{section*.18}
\contentsline {section}{Bibliograhy}{34}{section*.19}
\contentsline {section}{7. Appendix}{39}{section*.20}

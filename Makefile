diff: diff.pdf

diff.pdf: version_set2015.tex manuscrito.tex
	latexdiff version_dic.tex manuscrito.tex > diff.tex
	pdflatex diff.tex
	pdflatex diff.tex
	bibtex diff.aux
	pdflatex diff.tex
	pdflatex diff.tex
	pdflatex diff.tex
